package com.truckdiagonstic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.truckdiagonstic.R;
import com.truckdiagonstic.util.AppConstants;
import com.truckdiagonstic.util.CommonUtils;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

public class SplashActivity extends AppCompatActivity {
    ProgressBar pb;
    TextView tv;
    int prg = 0;
    private ProgressBar pbHorizontal = null;
    private TextView tvProgressHorizontal = null;
    private TextView tvProgressCircle = null;
    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext=SplashActivity.this;
        pb = (ProgressBar) findViewById(R.id.pbId);

        PulsatorLayout pulsator = (PulsatorLayout) findViewById(R.id.pulsator);
        pulsator.start();

       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }, 3000);
*/
        new Thread(myThread).start();


    }

    private Runnable myThread = new Runnable()
    {
        @Override
        public void run()
        {
            while (prg <= 100)
            {
                try
                {
                    hnd.sendMessage(hnd.obtainMessage());
                    Thread.sleep(50);
                }
                catch(InterruptedException e)
                {
                    Log.e("ERROR", "Thread was Interrupted");
                }
            }

            runOnUiThread(new Runnable() {
                public void run() {

                    startActivity(new Intent(mContext,LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();

                    /*
                    if(CommonUtils.getPreferencesBoolean(mContext, AppConstants.FIRST_TIME_LOGIN)){
                        startActivity(new Intent(mContext,LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();

                    }
                    else {
                        startActivity(new Intent(mContext,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();

                    }*/


                    // tv.setText("Finished");
                }
            });
        }



        Handler hnd = new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                prg++;
                pb.setProgress(prg);

                String perc = String.valueOf(prg).toString();
                if(prg<=100){
                   // tv.setText(prg+"%");
                }
            }
        };

    };
}
