package com.truckdiagonstic.activity;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.truckdiagonstic.R;
import com.truckdiagonstic.adapters.UserListAdapter;
import com.truckdiagonstic.model.UserListModel;

import java.util.ArrayList;
import java.util.List;


public class UserListActivity extends AppCompatActivity {
    RecyclerView chatrecylerview;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    List<UserListModel> userList = new ArrayList<>();
    UserListModel userListModel;
    private String user_id = "";
    UserListAdapter userListAdapter;
    String[] WALK_THROUGH = new String[]{Manifest.permission.READ_CONTACTS};
    private String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS};
    Context mContext;
    ImageView ivBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_list_activity);

        mContext  = UserListActivity.this;
        ivBack=findViewById(R.id.ivBack);


       ivBack.setOnClickListener((View v) -> {
           // do something here
           onBackPressed();

       });
        chatrecylerview = (RecyclerView) findViewById(R.id.chatrecyler);

        userList=new ArrayList<>();
        userListModel=new UserListModel();
        userListModel.setId("1");
        userListModel.setUserMobile("9452132576");
        userListModel.setUserName("RAjesh Patel");
        userList.add(userListModel);

        userListModel=new UserListModel();
        userListModel.setId("2");
        userListModel.setUserMobile("7210569138");
        userListModel.setUserName("Rajesh");
        userList.add(userListModel);

        userListModel=new UserListModel();
        userListModel.setId("3");
        userListModel.setUserMobile("7836884454");
        userListModel.setUserName("Mr Zen Watt");
        userList.add(userListModel);

        userListAdapter = new UserListAdapter(userList, mContext,userList);
        chatrecylerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        chatrecylerview.setAdapter(userListAdapter);

       // callUserListApi();
       // setContentView(R.layout.user_list_activity);
    }
}
