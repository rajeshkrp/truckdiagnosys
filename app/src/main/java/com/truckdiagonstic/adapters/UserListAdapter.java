package com.truckdiagonstic.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.truckdiagonstic.R;

import com.truckdiagonstic.chat_ui.ChatActivity;
import com.truckdiagonstic.model.UserListModel;
import com.truckdiagonstic.retrofit.ApiInterface;
import com.truckdiagonstic.util.AppConstants;
import com.truckdiagonstic.util.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Admin on 11/3/2017.
 */


public class UserListAdapter extends  RecyclerView.Adapter<UserListAdapter.ViewHolder>  {
    Dialog mDialog;
    ImageView pro_pic;
    TextView userName;
    boolean isImageFitToScreen;
    private int user_id = 0;
    List<UserListModel> arrayList = new ArrayList<>();
    Context context;
    private ApiInterface mInterface;

    private List<UserListModel> bothList;




    public UserListAdapter(List<UserListModel> arrayList, Context activity , List<UserListModel> bothList) {
        this.arrayList = arrayList;
        this.bothList = bothList;
        this.context = activity;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row_item, parent, false);
        ViewHolder recyclerViewHolder = new ViewHolder(view);


        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final UserListModel modelDetail = arrayList.get(position);

      //  user_id = Integer.valueOf(CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
        if (!TextUtils.isEmpty(modelDetail.getUserName()) && modelDetail.getUserName() != null) {
            holder.userName.setText(CommonUtils.NameCaps(modelDetail.getUserName()));

        } else {
            String name="No name";
            if(modelDetail.getUserName()!=null && !modelDetail.getUserName().trim().equals("")){
                name=modelDetail.getUserName();
            }
            holder.userName.setText(name+ (modelDetail.getUserMobile()));
            holder.userName.setTextColor(context.getResources().getColor(R.color.redcolor));
        }

        if (!TextUtils.isEmpty(modelDetail.getUserMobile()) && modelDetail.getUserMobile() != null) {
            holder.status.setText(modelDetail.getUserMobile());
        } else {
            holder.status.setText("No Number");
        }
        if (!TextUtils.isEmpty(modelDetail.getUserPic()) && modelDetail.getUserPic() != null) {
            Picasso.with(context).load(modelDetail.getUserPic()).into(holder.profileImage);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


/*

                Intent intent=new Intent(context,ChatActivity.class);

                CommonUtils.savePreferencesString(context,AppConstants.USER_ID,modelDetail.getId());
                CommonUtils.savePreferencesString(context,AppConstants.USER_NAME,modelDetail.getUserName());
                CommonUtils.savePreferencesString(context,AppConstants.USER_MOBILE,modelDetail.getUserMobile());
                intent.putExtra("UserID",modelDetail.getId());
                intent.putExtra("UserName",modelDetail.getUserName());
                intent.putExtra("Mobile",modelDetail.getUserMobile());
                context.startActivity(intent);
*/


                Intent intent1 = new Intent(context, ChatActivity.class);
                intent1.putExtra(AppConstants.RECEIVER__USER_NAME2, modelDetail.getUserName());
                intent1.putExtra(AppConstants.RECEIVER__PIC, modelDetail.getUserPic());
                intent1.putExtra(AppConstants.RECEIVER_MOBILE, modelDetail.getUserMobile());
               //intent1.putExtra(AppConstants.RECEIVER_ID, modelDetail.getId());
                context.startActivity(intent1);
                ((Activity)context).finish();

            }
        });
       /* holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(context);
                mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
                mDialog.setCanceledOnTouchOutside(true);
                mDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mDialog.getWindow().setGravity(Gravity.CENTER);
                WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
                lp.dimAmount = 0.85f;
                mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.getWindow();
                mDialog.getWindow().setAttributes(lp);
                View dialoglayout = inflater.inflate(R.layout.popup_profile_pic, null);
                mDialog.setContentView(dialoglayout);
                pro_pic = (ImageView) mDialog.findViewById(R.id.popup_pro_pic);
                userName = (TextView) mDialog.findViewById(R.id.userName);
                if (!TextUtils.isEmpty(modelDetail.getUserPic()) && modelDetail.getUserPic() != null) {

                    Picasso.with(context).load(modelDetail.getUserPic()).into(pro_pic);
                }
                if (!TextUtils.isEmpty(modelDetail.getUserName()) && modelDetail.getUserName() != null) {
                    userName.setText(CommonUtils.NameCaps(modelDetail.getUserName()));

                }
                // pro_pic.setImageResource(R.id.);
                mDialog.show();
                pro_pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, PopUpUserInfo.class);
                        intent.putExtra("USERNAME", modelDetail.getUserName());
                        intent.putExtra("PROFILEPIC", modelDetail.getUserPic());
                        mDialog.dismiss();
                        context.startActivity(intent);
                    }
                });

            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, status, timing,count;
        LinearLayout linearLayout,ll_row_container;
        CircleImageView profileImage;
        public ViewHolder(View itemView) {
            super(itemView);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
            ll_row_container = (LinearLayout) itemView.findViewById(R.id.ll_row_container);
            userName = (TextView) itemView.findViewById(R.id.chat_user_name);
            status = (TextView) itemView.findViewById(R.id.chat_status);
            timing = (TextView) itemView.findViewById(R.id.time);
            profileImage = (CircleImageView) itemView.findViewById(R.id.chat_profile_image);
            count= (TextView) itemView.findViewById(R.id.count);

        }
    }


}

