package com.truckdiagonstic.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.truckdiagonstic.R;
import com.truckdiagonstic.activity.MainActivity;
import com.truckdiagonstic.retrofit.ApiInterface;
import com.truckdiagonstic.retrofit.RetrofitHandler;
import com.truckdiagonstic.util.AppConstants;
import com.truckdiagonstic.util.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {

    @BindView(R.id.textInputET_c_pass)
    TextInputEditText textInputET_c_pass;

    @BindView(R.id.textInputET_name)
    TextInputEditText textInputET_name;

    @BindView(R.id.textInputET_phone)
    TextInputEditText textInputET_phone;

    @BindView(R.id.textInputET_pass)
    TextInputEditText textInputET_pass;

    @BindView(R.id.btn_submit)
    Button btn_submit;

    @BindView(R.id.tv_login)
    TextView tv_login;
    String device_token="";
    Context mContext;
    private static final String TAG = "RegisterFragment";
 /*   @BindView(R.id.tv_register)
    TextView tv_register;
*/
    private OnFragmentInteractionListener mListener;
    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        mContext=getActivity();
       device_token=  FirebaseInstanceId.getInstance().getToken();
       CommonUtils.savePreferencesString(mContext,AppConstants.DEVICE_TOKEN,device_token);
        Log.d(TAG, "device_token: "+ device_token);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        textInputET_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        textInputET_pass.setSelection(textInputET_pass.getText().length());

        textInputET_c_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        textInputET_c_pass.setSelection(textInputET_c_pass.getText().length());



        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager=getFragmentManager();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.replace(R.id.ll_container,new LoginFragment());
                transaction.commit();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(textInputET_name.getText().toString())){
                    CommonUtils.snackBar("Please Enter User Name ",textInputET_name);
                }else if(TextUtils.isEmpty(textInputET_phone.getText().toString())){
                    CommonUtils.snackBar("Please Enter Your Phone Number ",textInputET_name);
                }/*else if(textInputET_phone.length()<=10 && textInputET_phone.length()>10){
                    CommonUtils.snackBar("Please Enter Ten Digit Mobile No",textInputET_name);}*/
                else if(TextUtils.isEmpty(textInputET_pass.getText())){
                    CommonUtils.snackBar("Please Enter Password",textInputET_name);
                }else if(TextUtils.isEmpty(textInputET_c_pass.getText())){
                    CommonUtils.snackBar("Please Re-Enter Password",textInputET_name);
                }else if(!textInputET_pass.getText().toString().equals(textInputET_c_pass.getText().toString())){
                    CommonUtils.snackBar("Password does Not Match ",textInputET_name);
                }else {
                    String name=textInputET_name.getText().toString();
                    String phone=textInputET_phone.getText().toString();
                    String pass=textInputET_pass.getText().toString();
                    String c_pass=textInputET_c_pass.getText().toString();
                    if(name!=null&& phone!=null && pass!=null && c_pass!=null){

                    apiRegistration(name,phone,pass,c_pass,device_token);}

                }

            }
        });

    }

    private void apiRegistration(String name,String phone,String pass,String c_pass,String device_token) {
        ApiInterface apiService = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = apiService.registration(name,phone,pass,c_pass,device_token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                String str = null;
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    Toast.makeText(mContext, jObjError.getString("msg"), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    //Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                String msg = "";
                int status=0;
                try {
                    JSONObject jsonObject=new JSONObject(str);
                    msg=jsonObject.getString("msg");
                    status=jsonObject.getInt("status");
                    Log.d(TAG, "status: "+status);
                    Log.d(TAG, "onResponse: "+msg);

                    if(status==1){
                        CommonUtils.snackBar(msg,textInputET_c_pass);
                        startActivity(new Intent(mContext,MainActivity.class));
                        getActivity().finish();
                    }else {
                        CommonUtils.snackBar(msg,textInputET_c_pass);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });


    }

   /* @OnClick(R.id.tv_register)
    void submitbtn(){

    }
*/
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
