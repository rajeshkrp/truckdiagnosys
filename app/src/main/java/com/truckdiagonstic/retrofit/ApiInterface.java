package com.truckdiagonstic.retrofit;



import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 *
 * "http://www.404coders.com/NetWrko/WebServices/chat/devicetoken_get.php"
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("user_registration.php")
    Call<ResponseBody> registration(@Field("name") String username,
                                @Field("phone") String phone,
                                @Field("password") String password,
                                @Field("confirm_password") String c_pass,
                                @Field("device_id") String device_token);

    @FormUrlEncoded
    @POST("user_login.php")
    Call<ResponseBody> getLogin(@Field("phone") String phone,
                                @Field("password")String pass);









/*

    @FormUrlEncoded
    @POST("userDetail.php")
    Observable<List<UserTokenModel>> getFcmToken(@Field("user_id") String user_id);
*/









}
