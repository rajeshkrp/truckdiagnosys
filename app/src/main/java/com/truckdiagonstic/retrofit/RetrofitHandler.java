package com.truckdiagonstic.retrofit;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHandler {

    private static RetrofitHandler uniqInstance;

  //  private final String BASE_URL = "http://wehyphens.com/hid_messenger/webservices/";
        // private final String BASE_URL = "http://wehyphens.com/satyaconnect/api/";
         private final String BASE_URL = "http://www.wehyphens.com/truck_diagnostic/webservices/";
    private ApiInterface apiInterface;

    public static synchronized RetrofitHandler getInstance() {
        if (uniqInstance == null) {
            uniqInstance = new RetrofitHandler();
        }
        return uniqInstance;
    }

    public static synchronized RetrofitHandler getNewInstanceOnLogin() {
        uniqInstance = new RetrofitHandler();
        return uniqInstance;
    }

    private void ApiClient() {
        try {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set up log type
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(40, TimeUnit.SECONDS)
                    .readTimeout(90, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            apiInterface = retrofit.create(ApiInterface.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ApiInterface getApi() {
        if (apiInterface == null) {
            uniqInstance.ApiClient();
        }
        return apiInterface;
    }

   /* Interceptor header = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            String firebaseKey=AppUtils.readStringFromPref(HappyApplication.getmAppContext(), FIREBASE_KEY);
            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader("Accept", "application/json");
            builder.addHeader(STR_API_KEY, API_KEY);
            if(firebaseKey==null){
                firebaseKey=" ";
            }
            builder.addHeader(STR_FIRE_KEY, firebaseKey);
            return chain.proceed(builder.build());
        }
    };*/
}
