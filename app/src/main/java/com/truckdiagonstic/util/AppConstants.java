package com.truckdiagonstic.util;

import android.support.v4.app.Fragment;

public class AppConstants extends Fragment {
    public static final int MULTI_RESPONSE_CODE = 11;
    public static  final String IS_LOGIN = "false";
    public static  final String FIRST_TIME_LOGIN = "false";

    public static final String CURRENT_LONGI = "current_longi";
    public static String LAT = "lat";
    public static String LONGI = "longi";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "mobile";
    public static final String PROFILE_PIC = "profile_pic";


    public static final String CurrentCity="city_name";
    public static final String StateName ="country_name";
    public static final String RECEIVER_USER_ID ="sender_id";
    public static final String RECEIVER_MOBILE ="sender_id";
    public static final String RECEIVER__USER_NAME2 ="sender_name";
    public static final String RECEIVER__PIC ="receiver_pic";
    public static final String CURRENT_USER ="current_user";
  //  public static final String HASH_KEY ="hash_key";
    public static final String HASHTAG_KEY = "hashtag";
    public static final String ALL_HASHTAG_KEY = "hashtagkey";
    public static final String NOTIFICATION_KEY = "notification";
    public static final String FOLLOWED_POST_KEY = "followed_post";


    public static final int FRAGMENT_HOME=0;
    public static final int FRAGMENT_NOTIFICATION=1;
    public static final int FRAGMENT_ALL=2;


    public static final int FRAGMENT_POSTS=0;
    public static final int FRAGMENT_POLLS=1;
    public static final int FRAGMENT_FOLLOED_POSTS=2;
    public static final int FRAGMENT_FOLLOWED_POLLS=3;
    public static final int FRAGMENT_COMMENT_REPLY=4;

    /*For calling */

    public static final String SENDER_CALLER_ID ="callerId";
    public static final String SENDER_RECEIVER_ID ="callerId";
    public static final String RECEIVER_CALLER_ID ="recipientId";
    public static final String RECEIVER_ID ="receiver_id";
    public static final String DEVICE_TOKEN ="device_token";
    public static final String PHONE_CALL_STATUS ="status";




    public static class SharedPrefKeys {
        public static final String MY_SHAREDPREF_NAME = "com.netwrko";
        public static final String contentType = "application/json";



    }

}
