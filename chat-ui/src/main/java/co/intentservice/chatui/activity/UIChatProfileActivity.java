package co.intentservice.chatui.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import co.intentservice.chatui.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UIChatProfileActivity extends AppCompatActivity {
    Context mContext;
    String user_name="",filePath="",time="";
    int userstatus=0;
    TextView tvUser,tvStatusTime;
    ImageView iv_profile_pic,leftarrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_ui_profile);

        mContext=UIChatProfileActivity.this;
        tvStatusTime=(TextView)findViewById(R.id.tvStatusTime);
        tvUser=findViewById(R.id.tvNameUser);
        iv_profile_pic=findViewById(R.id.iv_profile_pic);
        leftarrow=findViewById(R.id.leftarrow);



        if (getIntent().getStringExtra("PATH") != null) {

            filePath = getIntent().getStringExtra("PATH");
            if (filePath != null&&!TextUtils.isEmpty(filePath)) {
                Picasso.with(mContext).load(filePath).into(iv_profile_pic);
                //   Picasso.with(context).load(path).into(binding.ivChat);
            }

        }
        if (getIntent().getStringExtra("NAME") != null) {

            user_name = getIntent().getStringExtra("NAME");
            tvUser.setText(co.intentservice.chatui.activity.CommonUtils.NameCaps(user_name));

        }
        if (getIntent().getStringExtra("TIME") != null) {
            time = getIntent().getStringExtra("TIME");
            tvStatusTime.setText(time);
        }
        if (getIntent().getStringExtra("READSATUS") != null) {

            userstatus = getIntent().getIntExtra("READSATUS",0);
        }





        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
    }
}
